# IOT Workshop

## Willkommen

## Definition: IOT (Versuch einer Definition)

Wiki: Das Internet der Dinge (IdD) (auch: „Allesnetz“;[1] englisch Internet of Things, Kurzform: IoT) ist ein Sammelbegriff für Technologien einer globalen Infrastruktur der Informationsgesellschaften, die es ermöglicht, physische und virtuelle Objekte miteinander zu vernetzen und sie durch Informations- und Kommunikationstechniken zusammenarbeiten zu lassen.
(https://de.wikipedia.org/wiki/Internet_der_Dinge)


Anbindung/Digitalisierung von physikalischen Messgrößen an das Internet/Netz - zur automatisierten Erfassung und Auswertung.

Anwendungsbeispiele: Erfassung von Daten z.B. in der Produktion (Stichwort: Smart Factory):
- Gewicht von Box mit Schrauben (Waage)
- Karton mit Teilen bereit / Palette da (Lichtschranke oder Abstandmessung)
- Temperatur
- Helligkeit

"Smart": Durch die Digitalisierung ist eine "Vermischung" von Informationen möglich, die auf unterschieldiche Arten gewonnen wurden.
z.B. Entscheidung ob Garten-Wasser-Sprenger angeschaltet werden muss - Messung der Bodenfeucht  UND die aktuelle Information aus dem Internet über die Regenwahrscheinlichkeit in den nächsten 12 Stunden.


Wie können wir Zustände der realen Welt aufnehmen und ins "Netz" schicken?
Microcontroller oder Mini-Computer können mit Sensoren Daten erfassen, bei Bedarf verarbeiten und ins Internet verteilen.


"Das Internet" - "Cloud" - wo liegen meine Daten?
Die Verbindung mehrer Rechner und die Kommunikation zwischen ihnen stellt ein Netzwerk dar.
Dabei können die verschiedenen Rechner Aufgaben übernehmen, wie z.B. Daten speichern, verarbeiten und zur Verfügung stellen.
Es gibt ein Netzwerk, mit dem sich jeder verbinden kann - das Internet. Jeder kann dort einen Service (z.B einen Webserver) bereitstellen oder Services aus diesem Netz nutzen.
Das gleiche kann aber auch in einem abgeschlossenen Netzwerk - meist internes Firmennetzwerk abgebildet werden.

Für diesen Workshop gibt es ein Netzwerk in dem sich mehrere Rechner befinden. z.B:
- Rechner/Micorcontroller, der Daten erfassen und verteilen kann
- Rechner der Daten empfangen/entgegennehmen kann (z.B. Datenbank) - und auf Anfrage wieder zur Vefügung stellt
- Rechner zum Anzeigen und Auswerten von Daten (z.B. über browser zugriff auf den Rechner mit den Daten)


Fokus im Workshop ist auf dem Rechner mit Sensoren zum erfassen der Daten

Beispiele für einen einfachen Einstieg zum Erfassen von Daten:
- Arduino (https://www.arduino.cc/en/Main/Products)
Microcontroller - externe Entwicklungsumgebung, hauptsächlich in C, keine Anschlüsse wie vom PC gewohnt, meisst zusätzliche Hardware nötig im mit einem Netzwerk zu verbinden
- ESP32 (https://www.espressif.com/en/products/socs/esp32) / Microcontroller - "minimales Betriebsystem", WLAN und Bluetooth
- Raspberry Pi / (https://www.raspberrypi.org/products/) / (Fast) vollwertiger Rechner mit Anschluß für Monitor, Linux-basiertes Betriebssystem


## Raspberry Pi
https://de.wikipedia.org/wiki/Raspberry_Pi

Der Raspberry Pi ist ein Einplatinencomputer in der Größe einer Kreditkarte.

Der im Vergleich zu üblichen Personal Computern sehr einfach aufgebaute Rechner wurde von der Stiftung mit dem Ziel entwickelt, jungen Menschen den Erwerb von Programmier- und Hardware-Kenntnissen zu erleichtern.


Linux-basiertes Betriebsystem mit der Möglichkeit einer grafischen Oberfläche.

Im Vergleich zu einem Standart-PC hat der RPi Anschlüsse für Sensoren, welche einfach in eine Vielzahl von Programmiersprachen eingebunden werden kann.



